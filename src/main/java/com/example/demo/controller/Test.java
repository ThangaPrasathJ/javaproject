package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Test {
    @GetMapping("test")
    public ResponseEntity<String> getTest(){
       String value="application testing";
        return new ResponseEntity<String>(value, HttpStatus.OK);
    }

}
